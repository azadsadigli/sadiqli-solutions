@extends('lts.ms')
@section('head')
<title>{{$search}}</title>
@if(count($sols) == 0)
<meta name="robots" content="noindex,nofollow,noarchive">
@else
<meta name="keywords" content="Axtarış nəticəsi: {{$search}}">
<meta name="description" content="@foreach($sols as $sol) {!! str_limit(strip_tags($sol->solution), $limit = 20, $end = '') !!}, @endforeach">
@endif
@endsection
@section('body')
<section class="section-t8 s-part">
  <div class="container">
    <div class="row">
        <form action="/search-result/search={request}" method="get" autocomplete="off" style="width:100%;">
          {{csrf_field()}}
          <input type="hidden" name="subcat_id" value="0">
          <div class="input-group">
            <input type="text" class="form-control" name="search" value="{{ isset($s) ?  $s : ''}}" oninvalid="this.setCustomValidity('Məlumat daxil edin')" oninput="setCustomValidity('')" placeholder="Search here..." required>
              <span class="input-group-btn">
                <button class="btn btn-success" type="submit"><i class="fa fa-search"></i> </button>
              </span>
          </div>
        </form>
    </div>
</div>
</section>
<section class="section-t8">
  <div class="container search-title">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h3 class="title-a">{{trans('app.resul_for',['search' => $search])}}</h3>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach($sols as $sol)
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-body-c sol-box">
            <p class="content-c">
              {!! str_limit(strip_tags($sol->solution), $limit = 150, $end = '...') !!}
            </p>
          </div>
          <hr>
          <div class="card-footer-c">
            <a href="/solution/{{$sol->slug}}" class="link-c link-icon" title="{{trans('app.read_more_detailed')}}">{{trans('app.more')}}
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

@if(2==3)
<section class="section-news section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Latest News</h2>
          </div>
          <div class="title-link">
            <a href="blog-grid.html">All News
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id="new-carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-2.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">House</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">House is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-5.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-7.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Park</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Park is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-3.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="#">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
@endsection
@section('foot')
@endsection
