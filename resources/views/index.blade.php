@extends('lts.ms')
@section('head')
<meta content="solutions, problems, solve, məsələlər, həllər, it-texnologiyaları, texnologiya" name="keywords">
<meta content="Texniki məsələlərin Azərbaycan dilində həlləri" name="description">
<title>{{trans('app.home')}} - Sadiqli Solutions</title>
@endsection
@section('body')
<div class="intro intro-carousel">
  <div id="carousel" class="owl-carousel owl-theme">
    <div class="carousel-item-a intro-item bg-image" style="background-image: url(//cdn.sadiq.li/img/sl1.jpg)">
      <div class="overlay overlay-a"></div>
      <div class="intro-content display-table">
        <div class="table-cell">
          <div class="container">
            <div class="row">
              <div class="col-lg-8">
                <div class="intro-body">
                  <h2 class="intro-title mb-4">
                    <span class="color-b">{{trans('app.from_here')}} </span>
                    <br> {{trans('app.search_here')}}</h2>
                  <p class="intro-subtitle intro-price">
                    <form action="/search-result/search={request}" method="get" autocomplete="off" >
                      {{csrf_field()}}
                      <input type="hidden" name="subcat_id" value="0">
                      <div class="input-group">
                        <input type="text" class="form-control" name="search" value="{{ isset($s) ?  $s : ''}}" oninvalid="this.setCustomValidity('{{trans('app.search')}}')" oninput="setCustomValidity('')" placeholder="Search here..." required>
                          <span class="input-group-btn">
                            <button class="btn btn-success" type="submit">{{trans('app.search')}}</button>
                          </span>
                      </div>
                    </form>
                  </p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
<section class="section-services section-t8 h-sec">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">{{trans('app.Solutions')}}</h2>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      @foreach($sols = App\Sols::orderBy('created_at','desc')->get() as $sol)
      <div class="col-md-4">
        <div class="card-box-c foo">
          <div class="card-body-c sol-box">
            <p class="content-c">
              {!! str_limit(strip_tags($sol->solution), $limit = 150, $end = '...') !!}
            </p>
          </div>
          <hr>
          <div class="card-footer-c">
            <a href="/solution/{{$sol->slug}}" class="link-c link-icon" title="{{trans('app.read_more_detailed')}}">{{trans('app.more')}}
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
      @endforeach
    </div>
  </div>
</section>

@if(2==3)
<section class="section-news section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Latest News</h2>
          </div>
          <div class="title-link">
            <a href="blog-grid.html">All News
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id="new-carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-2.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">House</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">House is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-5.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-7.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Park</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Park is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-3.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="#">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
@endsection
@section('foot')
@endsection
