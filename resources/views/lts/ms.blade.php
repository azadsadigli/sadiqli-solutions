<!DOCTYPE html>
<html lang="{{ config('app.locale')}}">
<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta name="author" content="Azad Sadigli">
  <meta name="_token" content="{{csrf_token()}}" />
@section('head')
@show
  <link href="//cdn.sadiq.li/img/logo.png" rel="icon">
  <link rel="stylesheet" href="//cdn.sadiq.li/css/sadsol.min.css">
</head>
<body>
  <div class="click-closed"></div>
  @if(1==3)
  @include('lts.search')
  @endif
  <nav class="navbar navbar-default navbar-trans navbar-expand-lg fixed-top">
    <div class="container">
      <button class="navbar-toggler collapsed" type="button" data-toggle="collapse" data-target="#navbarDefault"
        aria-controls="navbarDefault" aria-expanded="false" aria-label="Toggle navigation">
        <span></span>
        <span></span>
        <span></span>
      </button>
      <a class="navbar-brand text-brand-logo" href="/">SAD<span class="color-b">SOLUTIONS</span></a>
      <div class="navbar-collapse collapse justify-content-center" id="navbarDefault">
        <ul class="navbar-nav">
          <li class="nav-item">
            <a class="nav-link @if(Request::is('/')) active @endif" href="/">{{trans('app.home')}}</a>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              {{trans('app.categories')}}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              @foreach($cats = App\Category::all() as $cat)
               <a class="dropdown-item" href="/category/{{$cat->slug}}">{{$cat->name}}</a>
              @endforeach
            </div>
          </li>
          <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown"
              aria-haspopup="true" aria-expanded="false">
              {{trans('app.more')}}
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown">
              <a class="dropdown-item" href="https://sadiqli.com/all-blogs">{{trans('app.all_blogs')}}</a>
                @foreach($pgs = App\Page::where('active',1)->where('sol_header',1)->get() as $pg)
                <a class="dropdown-item capi" href="https://sadiqli.com/page/{{$pg->slug}}">{{$pg->short_name}}</a>
                @endforeach
              <a class="dropdown-item" href="https://sadiqli.com/contact">{{trans('app.contact')}}</a>
            </div>
          </li>
          <li class="nav-item">
            <a class="nav-link" href="https://sadiqli.com/suggest-new-topic">{{trans('app.send_problem')}}</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>
  @section('body')
  @show
  <section class="section-footer">
    <div class="container">
      <div class="row">
        <div class="col-sm-12 col-md-4 section-md-t3">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">{{trans('app.information')}}</h3>
            </div>
            <div class="w-body-a">
              <div class="w-body-a">
                <ul class="list-unstyled capi">
                  @foreach($pgs = App\Page::where('active',1)->where('sol_footer',1)->where('footer_loc',"info")->get() as $pg)
                  <li class="item-list-a"><a class="capi" href="https://sadiqli.com/page/{{$pg->slug}}">{{$pg->short_name}}</a></li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
        <div class="col-sm-12 col-md-4 section-md-t3">
          <div class="widget-a">
            <div class="w-header-a">
              <h3 class="w-title-a text-brand">{{trans('app.guidance')}}</h3>
            </div>
            <div class="w-body-a">
              <div class="w-body-a">
                <ul class="list-unstyled capi">
                  <li class="item-list-a"><a class="capi" href="https://sadiqli.com/suggest-new-topic">{{trans('app.send_problem')}}</a></li>
                  @foreach($pgs = App\Page::where('active',1)->where('sol_footer',1)->where('footer_loc',"guid")->get() as $pg)
                  <li class="item-list-a"><a class="capi" href="https://sadiqli.com/page/{{$pg->slug}}">{{$pg->short_name}}</a></li>
                  @endforeach
                </ul>
              </div>
            </div>
          </div>
        </div>
          <div class="col-sm-12 col-md-4">
            <div class="widget-a">
              <div class="w-header-a">
                <h3 class="w-title-a text-brand">SadSolutions</h3>
              </div>
              <div class="w-footer-a">
                <ul class="list-unstyled">
                  <li class="color-a">
                    <span class="color-text-a">{{trans('app.email')}}: </span> contact@sadiqli.com</li>
                    <li>
                      <div class="hidden_alert alert alert-success" id="subs_sent" style="display:none;">{{trans('app.subscibe_success_text')}} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
                      <div class="hidden_alert alert alert-danger" id="subs_send_failed" style="display:none;">{{trans('app.subscibe_failed_text')}} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
                      <div class="input-group">
                        <input type="text" class="form-control" id="SoSuEmail" placeholder="{{trans('app.email')}}...">
                          <span class="input-group-btn">
                            <button class="btn btn-success" id="SoSuEmailAdd" type="submit">{{trans('app.subscribe')}}</button>
                          </span>
                      </div>
                  </li>
                </ul>
              </div>
            </div>
          </div>
      </div>
    </div>
  </section>
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="socials-a">
            <ul class="list-inline">
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-facebook" aria-hidden="true"></i>
                </a>
              </li>
              <li class="list-inline-item">
                <a href="#">
                  <i class="fa fa-instagram" aria-hidden="true"></i>
                </a>
              </li>
            </ul>
          </div>
          <div class="copyright-footer">
            <p class="copyright color-text-a">
              &copy;
              <span class="color-a">SadSolutions</span>
            </p>
          </div>
        </div>
      </div>
    </div>
  </footer>
  <a href="#" class="back-to-top"><i class="fa fa-chevron-up"></i></a>
  <script type="text/javascript" src="//cdn.sadiq.li/js/sadsol.min.js"></script>
@section('foot')
@show
</body>
</html>
