@extends('lts.ms')
@section('head')
<meta name="description" content="{{strip_tags($sols->solution)}}">
<meta name="keywords" content="@foreach($tag = App\Tag::where('problem_id',$sols->id)->get() as $ta) {{ $loop->first ? '' : ', ' }}{{$ta->tag}} @endforeach">
<title>{{$sols->title}}</title>
@endsection
@section('body')
  <section class="news-single nav-arrow-b">
    <div class="container">
      <div class="row">
        <div class="col-md-10 offset-md-1 col-lg-8 offset-lg-2">
          <div class="post-information">
            <ul class="list-inline  color-a">
              <li class="list-inline-item mr-2">
                <strong>{{trans('app.author')}}: </strong>
                <span class="color-text-a">Admin</span>
              </li>
              <li class="list-inline-item mr-2">
                <strong>{{trans('app.category')}}: </strong>
                @php $catrel = App\Catrelation::where('problem_id',$sols->id)->get() @endphp
                @foreach($catrel as $ct)
                  @foreach($cats = App\Category::where('id',$ct->cat_id)->get() as $cat)
                  <span class="color-text-a"><a href="/category/{{$cat->slug}}" title="{{$cat->name}}">{{$cat->name}}</a> </span>
                  @endforeach
                @endforeach
              </li>
              <li class="list-inline-item">
                <strong>{{trans('app.date')}}: </strong>
                <span class="color-text-a">{{$sols->created_at->format('d M, Y')}}</span>
              </li>
            </ul>
          </div>

          <div class="post-content color-text-a">
            <p class="post-intro">{{$sols->title}}</p>
            <p>{!! $sols->solution !!}</p>
          </div>
          <div class="post-footer">
            <div class="post-share" id="Solidiv">
              <ul class="list-inline socials">
                <li class="list-inline-item">
                  <a href="#SolButModal" data-toggle="modal">
                    <i class="fa fa-thumbs-up" aria-hidden="true"></i></a> {{$like = App\Like::where('problem_id',$sols->id)->count()}}
                </li>
                <li class="list-inline-item">
                  <a><i class="fa fa-comments" aria-hidden="true"></i> {{$comm = App\Comment::where('problem_id',$sols->id)->count()}}</a>
                </li>
                @if($tag = App\Tag::where('problem_id',$sols->id)->count() != 0)
                <li class="list-inline-item">
                    <i class="fa fa-hashtag" aria-hidden="true"></i>
                      @foreach($tag = App\Tag::where('problem_id',$sols->id)->get() as $ta)<a href="/similar-solutions/{{$ta->tag}}" title="{{$ta->tag}}">{{ $loop->first ? '' : ', ' }}{{$ta->tag}}</a>@endforeach
                </li>
                @endif
              </ul>
            </div>
          </div>
          <div class="modal fade" id="SolButModal" role="dialog">
            <div class="modal-dialog">
              <div class="modal-content">
                <div class="modal-header">
                    <a class="close" data-dismiss="modal" aria-label=""><span>×</span></a>
                 </div>
                 <div class="modal-body">
                   <div class="like_div">
                     <h4> {{trans('app.please_fill_blanks')}} </h4>
                     <div class="hidden_alert_like alert alert-danger" id="solution_like_failed" style="display:none;">{{trans('app.failed_text_message')}}</div>
                     <input type="hidden" id="sol_id" value="{{$sols->id}}">
                     <input type="text" id="solution_liker" class="form-control" placeholder="* {{trans('app.enter_name')}}"><br>
                     <input type="text" id="esolution_liker" class="form-control" placeholder="* {{trans('app.enter_email_address')}}"><br>
                     <a id="LikeSolution" class="btn btn-success pull-right">{{trans('app.like_it')}}</a><br><br>
                   </div>
                   <div class="thank-you-pop" id="solution_liked" style="display:none;">
                      <img src="/img/success.png" alt="Thank you!">
                      <h1>{{trans('app.thank_you')}}</h1>
                      <p> {{trans('app.your_submission_is_received')}}</p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-10 offset-md-1 col-lg-10 offset-lg-1">
          <div class="title-box-d" id="SCommentNumb">
            <h3 class="title-d">{{trans('app.comments')}} ({{$coms = App\Comment::where('problem_id',$sols->id)->count()}})</h3>
          </div>
          <div class="box-comments" id="SCommentSuccess">
            <ul class="list-comments">
              @foreach($coms = App\Comment::where('problem_id',$sols->id)->whereNull('reply_id')->get() as $com)
              <li>
                <div class="comment-avatar">
                  <img src="//sadiqli.com/img/avatar.png" alt="{{$com->name}}" title="{{$com->name}}">
                </div>
                <div class="comment-details">
                  <h4 class="comment-author">{{$com->name}}</h4>
                  <span>{{$com->created_at->format('d M, Y')}}</span>
                  <p class="comment-description">{{$com->comment}}</p>
                </div>
              </li>
                @foreach($cos = App\Comment::where('reply_id',$com->id)->get() as $co)
                <li class="comment-children">
                  <div class="comment-avatar">
                    <img src="//sadiqli.com/img/avatar.png" alt="{{$co->name}}" title="{{$co->name}}">
                  </div>
                  <div class="comment-details">
                    <h4 class="comment-author">{{$co->name}} <i class="v-prof">&#x2714;</i> </h4>
                    <span>{{$co->created_at->format('d M, Y')}}</span>
                    <p class="comment-description">
                      {{$com->comment}}
                    </p>
                  </div>
                </li>
                @endforeach
              @endforeach
            </ul>
          </div>
          <div class="form-comments">
            <div class="title-box-d">
              <h3 class="title-d"> {{trans('app.Leave_a_Reply')}}</h3>
            </div>
            <div class="hidden_alert alert alert-success alert-dismissible" id="scomment_sent" style="display:none;">{{trans('app.your_submission_is_received')}} {{trans('app.thank_you')}}<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
            <div class="hidden_alert alert alert-danger alert-dismissible" id="scomment_sent_failed" style="display:none;">{{trans('app.failed_text_message')}} <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a></div>
            <form class="form-a com-form">
              <div class="row">
                <input type="hidden" id="comment_sol_id" value="{{$sols->id}}">
                <div class="col-md-6 mb-3">
                  <div class="form-group">
                    <input type="text" class="form-control form-control-lg form-control-a" id="scomment_name" placeholder="* {{trans('app.enter_name')}}" required>
                  </div>
                </div>
                <div class="col-md-6 mb-3">
                  <div class="form-group">
                    <input type="email" class="form-control form-control-lg form-control-a" id="scomment_email" placeholder="* {{trans('app.enter_email_address')}}" required>
                  </div>
                </div>
                <div class="col-md-12 mb-3">
                  <div class="form-group">
                    <textarea id="scomment_body" class="form-control" placeholder="* {{trans('app.your_comment_here')}}" cols="45" rows="8" required></textarea>
                  </div>
                </div>
                <div class="col-md-12">
                  <button type="submit" id="AddSolComment" class="btn btn-success pull-right">{{trans('app.send')}}</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
@section('foot')
@endsection
