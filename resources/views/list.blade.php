@extends('lts.ms')
@section('head')
<meta content="solutions, problems, solve, məsələlər, həllər, it-texnologiyaları, texnologiya" name="keywords">
<meta content="@if(Request::is('similar-solutions/*')) {{$tagn->tag}} @else {{$cat->name}} @endif - Sadiqli Solution (Həllər)" name="description">
<title>@if(Request::is('similar-solutions/*')) {{$tagn->tag}} @else {{$cat->name}} @endif</title>
@endsection
@section('body')
<section class="section-services section-t8 list-page">
  <div class="container search-title">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h3 class="title-a"><i>@if(Request::is('similar-solutions/*')) {{trans('app.tag')}}: {{$tagn->tag}} @else {{trans('app.category')}}: {{$cat->name}} @endif</i></h3>
          </div>
        </div>
      </div>
    </div>
    <div class="row">
      @if(Request::is('similar-solutions/*'))
      @foreach($tags as $tag)
        @foreach($sols = App\Sols::where('id',$tag->problem_id)->get() as $sol)
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-body-c sol-box">
              <p class="content-c">
                {!! str_limit(strip_tags($sol->solution), $limit = 150, $end = '...') !!}
              </p>
            </div>
            <hr>
            <div class="card-footer-c">
              <a href="/solution/{{$sol->slug}}" class="link-c link-icon" title="{{trans('app.read_more_detailed')}}">{{trans('app.more')}}
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
        @endforeach
        @endforeach
      @else
      @foreach($cts as $ct)
        @foreach($sols = App\Sols::where('id',$ct->problem_id)->get() as $sol)
        <div class="col-md-4">
          <div class="card-box-c foo">
            <div class="card-body-c sol-box">
              <p class="content-c">
                {!! str_limit(strip_tags($sol->solution), $limit = 150, $end = '...') !!}
              </p>
            </div>
            <hr>
            <div class="card-footer-c">
              <a href="/solution/{{$sol->slug}}" class="link-c link-icon" title="{{trans('app.read_more_detailed')}}">{{trans('app.more')}}
                <span class="ion-ios-arrow-forward"></span>
              </a>
            </div>
          </div>
        </div>
        @endforeach
      @endforeach
      @endif
    </div>
  </div>
</section>

@if(2==3)
<section class="section-news section-t8">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <div class="d-flex justify-content-between">
          <div class="title-box">
            <h2 class="title-a">Latest News</h2>
          </div>
          <div class="title-link">
            <a href="blog-grid.html">All News
              <span class="ion-ios-arrow-forward"></span>
            </a>
          </div>
        </div>
      </div>
    </div>
    <div id="new-carousel" class="owl-carousel owl-theme">
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-2.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">House</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">House is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-5.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-7.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Park</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="blog-single.html">Park is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="carousel-item-c">
        <div class="card-box-b card-shadow news-box">
          <div class="img-box-b">
            <img src="img/post-3.jpg" alt="" class="img-b img-fluid">
          </div>
          <div class="card-overlay">
            <div class="card-header-b">
              <div class="card-category-b">
                <a href="#" class="category-b">Travel</a>
              </div>
              <div class="card-title-b">
                <h2 class="title-2">
                  <a href="#">Travel is comming
                    <br> new</a>
                </h2>
              </div>
              <div class="card-date">
                <span class="date-b">18 Sep. 2017</span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
@endif
@endsection
@section('foot')
@endsection
