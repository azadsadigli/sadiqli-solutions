<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'Controller@index');
Route::get('/solution/{slug}', 'Controller@solution');
Route::post('/add/solution/comment','UserController@addsolcomment');
Route::get('/search-result/search={request}','DataController@searchsolution');
Route::post('/likesolution','UserController@likesolution');
Route::get('/category/{slug}','UserController@category');
Route::post('/add/new/subscriber','UserController@addsubscriber');
Route::get('/similar-solutions/{tag}','DataController@simsols');
