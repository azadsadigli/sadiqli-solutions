<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Comment;
use App\Notify;
use App\Like;
use App\Catrelation;
use App\Category;
use App\Subscription;
class UserController extends Controller
{
  public function category($slug){
    $cat = Category::where('slug',$slug)->first();
    $cts = Catrelation::where('cat_id',$cat->id)->get();
    return view('list',compact('cat','cts'));
  }
  public function addsolcomment(Request $req){
    $co = Comment::where('blog_id',$req->problem_id)->where('email',$req->email)->where('user_ip',\Request::ip())->first();
    if (empty($co)) {
      $comm = new Comment;
      $comm->name = $req->name;
      $comm->problem_id = $req->problem_id;
      $comm->reply_id = $req->reply_id;
      $comm->user_ip = \Request::ip();
      $comm->token = md5(microtime());
      $comm->comment = $req->comment;
      $comm->email = $req->email;
      $comm->save_me = 0;
      $comm->save();
      $com_m = Comment::orderBy('created_at','desc')->where('user_ip',\Request::ip())->first();
        $not = new Notify;
        $not->type = "comment";
        $not->comment_id = $com_m->id;
        $not->token = md5(microtime());
        $not->problem_id = $req->problem_id;
        $not->save();
      return response()->json(['success'=>'Data is successfully added','error' => 'Failed']);
    }else {
      $com = Comment::where('email',$req->email)->where('user_ip',\Request::ip())->orderBy('created_at','DESC')->first();
      $com->token = md5(microtime());
      $com->update();
      return response()->json(['success'=>'Data is successfully added','error' => 'Failed']);
    }
  }
  public function addsubscriber(Request $req){
    $sb = Subscription::where('type','=',"solution")->where('email',$req->email)->first();
    if (empty($sb)) {
      $subs = new Subscription;
      $subs->user_ip = \Request::ip();
      $subs->type = "solution";
      $subs->status = 1;
      $subs->email = $req->email;
      $subs->save();
        $not = new Notify;
        $not->type = "subscibe";
        $not->subscriber = $req->email;
        $not->token = md5(microtime());
        $not->save();
      return response()->json(['success','error']);
    }else{
      $sus = Subscription::where('id',$sb->id)->first();
      $sus->status = 1;
      $sus->update();
      return response()->json(['success','error']);
    }
  }
  public function likesolution(Request $req){
      $like_m = Like::where('email',$req->eblog_liker)->where('blog_id',$req->bl_id)->first();
      if (empty($like_m)) {
        $like = new Like;
        $like->problem_id = $req->sol_id;
        $like->commenter = $req->solution_liker;
        $like->email = $req->esolution_liker;
        $like->user_ip = \Request::ip();
        $like->token = md5(microtime());
        $like->save();
          $lk = Like::where('email',$req->solution_liker)->where('problem_id',$req->sol_id)->first();
          $not = new Notify;
          $not->type = "like";
          $not->like_id = $lk->id;
          $not->token = md5(microtime());
          $not->problem_id = $req->sol_id;
          $not->save();
        return response()->json(['success'=>'Data is successfully added','error' => 'Failed']);
      }else{
        $lik = Like::where('problem_id',$req->sol_id)->where('email',$req->solution_liker)->first();
        $lik->user_ip = \Request::ip();
        $lik->token = md5(microtime());
        $lik->update();
        return response()->json(['success'=>'Data is successfully added','error' => 'Failed']);
      }
  }
}
