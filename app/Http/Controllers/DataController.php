<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Search;
use Input;
use DB;
use App\Sols;
use App\Tag;

class DataController extends Controller
{
    public function simsols($tag){
      $tagn = Tag::where('tag',$tag)->first();
      $tags = Tag::where('tag',$tagn->tag)->get();
      return view('list',compact('tags','tagn'));
    }
  public function searchsolution(Request $req){
    $search = $req->search;
  $sr = Search::where('search_tag',$search)->where('type',"solution")->first();
  if (!empty($sr)) {
    $src_up = Search::where('id',$sr->id)->first();
    $src_up->count = $src_up->count + 1;
    $src_up->update();
  }else{
    $srch = new Search;
    $srch->search_tag = $search;
    $srch->type = "solution";
    $srch->save();
  }
  $suid = $req->sub_id;
  if($suid == 0){
    $sols = Sols::orderBy('created_at','desc')->where('title','LIKE','%' .$search. '%')
    ->orWhere('solution','LIKE','%' .$search. '%')
    ->get();
  }else{
    $sols = Sols::orderBy('created_at','desc')->where(function ($query) use ($search, $req){
          $query->where('subcat_id', $req->sub_id)
                ->where('title','LIKE','%' .$search. '%');
    })
    ->orWhere(function ($query) use ($search, $req){
          $query->where('subcat_id', $req->sub_id)
                ->where('solution','LIKE','%' .$search. '%');
    })->get();
  }
  return view('search-page',compact('sols','search'));
  }
}
